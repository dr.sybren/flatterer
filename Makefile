VERSION := $(shell git describe --tags --dirty --always)
DIST_FPATH := dist/flatterer-${VERSION}.zip
PACKAGE_PATH := flatterer

version:
	@echo "Version: ${VERSION}"
	@echo "Package: ${DIST_FPATH}"

.gitlabAccessToken:
	$(error gitlabAccessToken does not exist, visit Visit https://gitlab.com/profile/personal_access_tokens, create a Personal Access Token with API access then save it to the file .gitlabAccessToken)

release: .gitlabAccessToken ${DIST_FPATH}
	rsync ${DIST_FPATH} stuvel@stuvel.eu:files/flatterer/
	cd release && go run release.go -version ${VERSION} -fileglob ../${DIST_FPATH}

package:
	rm -rf ${PACKAGE_PATH} ${DIST_FPATH}
	${MAKE} -S -s ${DIST_FPATH}

# Remove old ZIP file as well, otherwise 7z will update it instead of overwriting.
${DIST_FPATH}:
	rm -rf ${PACKAGE_PATH} ${DIST_FPATH}
	mkdir -p ${PACKAGE_PATH} $(dir ${DIST_FPATH})
	rsync -av README.md LICENSE CHANGELOG.md "example box.blend" *.toml *.py *.egg ${PACKAGE_PATH}/
	python -m zipfile -c ${DIST_FPATH} ${PACKAGE_PATH}/*
	rm -rf ${PACKAGE_PATH}
	@echo Created ${DIST_FPATH}

.PHONY: version release package
